export class Todo {
    title: string;
    description: string;
    dueDate: string;
    status: string;
    id?: number;

    constructor(title: string, description: string, duedate: string, status: string) {
        this.title = title;
        this.dueDate = duedate;
        this.description = description;
        this.status = status;
    }
}

export class SortedTodoList {
    todo: Todo[];
    inprogress: Todo[];
    done: Todo[];

    constructor(todo: Todo[], inprogress: Todo[], done: Todo[]) {
        this.todo = todo;
        this.inprogress = inprogress;
        this.done = done;
    }
}


