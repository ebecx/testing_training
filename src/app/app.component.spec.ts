import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component;

  beforeEach(() => {
    component = new AppComponent();
  });

  afterEach(() => {
    component = null;
  });

  it('should set the correct title', () => {
    expect(component.title).toContain('summerschool');
  });
});
