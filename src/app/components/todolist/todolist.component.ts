import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SortedTodoList, Todo } from 'src/app/models/todo';
import { ModalService } from 'src/app/services/modal.service';
import { TodolistService } from 'src/app/services/todolist.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit {

  constructor(
    private todoService: TodolistService,
    private modalService: ModalService,
    ) { }

  //todolist: Todo[];
  sortedTodolist: SortedTodoList = new SortedTodoList([], [], []);
  subscription: Subscription;


  ngOnInit() {
    this.subscription = this.todoService.todoStore.subscribe(response => {
      this.sortTodoItems(response);
    });
    this.todoService.updateStore();
  }

  sortTodoItems(todoList: Todo[]) {
    this.sortedTodolist = {todo: [], inprogress: [], done: []};

    todoList.forEach(element => {
      if(element.status == "TODO") { this.sortedTodolist.todo.push(element) };
      if(element.status === "INPROGRESS") { this.sortedTodolist.inprogress.push(element) };
      if(element.status === "DONE") { this.sortedTodolist.done.push(element) };
    });
  }

  formatStatus(status): string {
    switch(status) {
      case "TODO": return 'Todo';
      case "INPROGRESS": return 'In Progress';
      case "DONE": return 'Done';
    }
  }

  viewTodoItem(id) {
    this.modalService.open('view-todo-item', id);
  }

  editTodoItem(id){
    this.modalService.open('edit-todo-item', id);
  }

  deleteTodoItem(id) {
    this.todoService.deleteTodoItem(id).subscribe(() => this.todoService.updateStore() );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
