import {Component, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import { Todo } from 'src/app/models/todo';
import { ModalService } from 'src/app/services/modal.service';
import { TodolistService } from 'src/app/services/todolist.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {

  display$: Observable<'open' | 'close'>;
  todoItem: Todo;
  todoItemId;

  constructor(
      private modalService: ModalService, private todoService: TodolistService
  ) {}

  ngOnInit() {
    this.display$ = this.modalService.watch();

    this.display$.subscribe(res => {
      if(res === "open") { this.renderView(this.getType()) }
    })
  }

  getType(): string {
    return this.modalService.getType();
  }

  renderView(type): void {
    switch(type) {

      case 'add-todo-item': {
        console.log("Add-todo-item received")
        this.todoItem = new Todo(null, null, null, null);
      }
      break;

      case 'edit-todo-item': {
        console.log("Edit-todo-item received")
        this.getSingleTodo(this.modalService.getTodoItemId());
      }
      break

      case 'view-todo-item': {
        console.log("View-todo-item received")
        this.getSingleTodo(this.modalService.getTodoItemId());
      }
      break;
    }
  }

  close(): void {
    this.modalService.close();
  }

  getSingleTodo(todoItemId): void {
    this.todoService.getSingleTodoItem(todoItemId).subscribe( (data: any) =>  { this.todoItem = data.body });
  }

  createTodo(): void {
    this.todoService.addTodoItem(this.todoItem).subscribe(() => {
      this.todoService.updateStore();
      this.modalService.close()
    })
  }

  updateTodo(): void {
    this.todoService.updateTodoItem(this.todoItem.id, this.todoItem).subscribe(() => {
      this.todoService.updateStore();
      this.modalService.close()
    })
  }
}
