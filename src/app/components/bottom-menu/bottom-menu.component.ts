import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo';
import { ModalService } from 'src/app/services/modal.service';
import { TodolistService } from 'src/app/services/todolist.service';

@Component({
  selector: 'app-bottom-menu',
  templateUrl: './bottom-menu.component.html',
  styleUrls: ['./bottom-menu.component.scss']
})
export class BottomMenuComponent implements OnInit {

  constructor(
    private todoService: TodolistService,
    private modalService: ModalService
    ) { }

  ngOnInit() {
  }

  addTodo() {
      this.modalService.open('add-todo-item');
  }

}
