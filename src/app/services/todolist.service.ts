import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Todo } from '../models/todo';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodolistService {

  constructor(private http: HttpClient) { }

  baseUrl = environment.baseUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
    observe: 'response' as 'body'
 };

  public todoStore = new Subject<Todo[]>();

  updateStore() {
    this.getAllTodos().subscribe((data: any) => {
      this.todoStore.next(data.body);
    });
  }

  private getAllTodos() {
    return this.http.get<Todo[]>(`${this.baseUrl}/todo/getAll`, this.httpOptions);
  }

  getSingleTodoItem(id) {
    return this.http.get<Todo>(`${this.baseUrl}/todo/get/${id}`, this.httpOptions);
  }

  addTodoItem(todo: Todo): Observable<any> {
    return this.http.post<Todo>(`${this.baseUrl}/todo/addToDoItem`, JSON.stringify(todo), this.httpOptions);
  }

  updateTodoItem(id, todo: Todo) {
    return this.http.put<Todo>(`${this.baseUrl}/todo/updateToDoItem/${id}`, JSON.stringify(todo), this.httpOptions);
  }

  deleteTodoItem(id) {
    return this.http.delete<Todo>(`${this.baseUrl}/todo/deleteToDoitem/${id}`, this.httpOptions);
  }
}
