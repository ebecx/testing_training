import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private userIsAuthenticated = false;

  constructor() { }

  public isAuthenticated(): boolean {
    return this.userIsAuthenticated;
  }

  public authenticateUser() {
    this.userIsAuthenticated = true;
  }
}
