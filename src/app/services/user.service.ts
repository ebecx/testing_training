import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  baseUrl = environment.baseUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
    observe: 'response' as 'response'
 };

  loginUser(loginDetails: User): Observable<any> {
    return this.http.post<User>(`${this.baseUrl}/user/login`, JSON.stringify(loginDetails), this.httpOptions);
  }

  registerUser(registerDetails): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/user/register`, JSON.stringify(registerDetails), this.httpOptions);
  }
}
