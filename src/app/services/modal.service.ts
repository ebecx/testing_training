import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private display: BehaviorSubject<'open' | 'close'> = new BehaviorSubject('close');
  private modalType;
  private todoItemId;

  watch(): Observable<'open' | 'close'> {
    return this.display.asObservable();
  }

  getType() {
    return this.modalType;
  }

  getTodoItemId() {
    return this.todoItemId;
  }

  open(modalType, todoItemId?) {
    this.modalType = modalType;
    if(todoItemId) { this.todoItemId = todoItemId};

    this.display.next('open');
  }

  close() {
    this.display.next('close');
  }
}
