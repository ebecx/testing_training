import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginDetails: User = new User('', '');
  loginMessage = { message: null, type: null };
  disabledButton: boolean = false;

  constructor(
    private userService: UserService,
    private auth: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    this.disabledButton = true;
    this.userService.loginUser(this.loginDetails).subscribe(
      {
        next: (res) => {
          if (res.status === 200) {
            this.authenticateUser();
          } else { throw Error; }
        },
        error: (err) => {
          err.status === 401 ? this.showErrorMessage("unauthorized") : this.showErrorMessage();
        }
      });
  }

  register() {
    this.router.navigate(['/register']);
  }

  authenticateUser() {
    this.loginMessage = { message: "Authorized user. Granting access..", type: "success" };

    setTimeout(() => {
      this.auth.authenticateUser();
      this.disabledButton = false;
      this.router.navigate(['/home']);
    }, 1500)
  }

  showErrorMessage(type: string = "generic") {
    this.loginMessage = { message: "Oops! Something went wrong! Please try again later.", type: "error" };
    this.disabledButton = false;

    if (type === "unauthorized") {
      this.loginMessage.message = "Unauthorized user. Wrong username or password.";
    }
  }
}
