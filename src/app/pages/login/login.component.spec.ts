import { TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';

describe('LoginComponent', () => {
  // Required for functions that have timeouts
  jest.useFakeTimers();
  let fixture;
  let component;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        RouterTestingModule.withRoutes([]),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture = null;
    router = null;
    component = null;
  });

  it('should render the login component', () => {
    expect(component).toBeTruthy();
  });

  it('should run ngOnOnit', () => {
    const onInitSpy = jest.spyOn(component, 'ngOnInit');
    component.ngOnInit();
    expect(onInitSpy).toHaveBeenCalledTimes(1);
  });

  it('should have a default empty loginMessage', () => {
    expect(component.loginMessage.message).toBeNull();
    expect(component.loginMessage.type).toBeNull();
  });

  it('should set error message on error', () => {
    component.showErrorMessage();
    expect(component.loginMessage.message).toContain('Oops!');
    expect(component.loginMessage.type).toStrictEqual('error');
  });

  it('should set error based on params', () => {
    component.showErrorMessage("unauthorized");
    expect(component.loginMessage.message).toContain('Unauthorized');
    expect(component.loginMessage.type).toStrictEqual('error');
  });

  it('should navigate to register page', () => {
    const routerSpy = jest.spyOn(router, 'navigate');
    component.register();
    expect(routerSpy).toHaveBeenCalledWith(['/register']);
  });

  it('should authenticate user', () => {
    const routerSpy = jest.spyOn(router, 'navigate');
    component.authenticateUser();
    jest.runAllTimers();
    expect(component.loginMessage.message).toContain('Granting access');
    expect(component.loginMessage.type).toStrictEqual('success');
    expect(routerSpy).toHaveBeenCalledWith(['/home']);
  });
});

