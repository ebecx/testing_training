import { Component } from '@angular/core';
import { zoomInOnEnterAnimation } from 'angular-animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [zoomInOnEnterAnimation()],
})
export class HomeComponent {
  constructor() { }

  pageTitle = 'Summerschool Todolist';

  testString() {
    return 'Test';
  }

  testSum(a, b) {
    if (typeof a === 'number' && typeof b === 'number') {
      return a + b;
    } else {
      return `${a} or ${b} was not a number.`;
    }
  }

  changeTitle() {
    this.pageTitle = 'New Page Title';
  }
}
