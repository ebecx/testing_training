import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component;

  beforeEach(() => {
    component = new HomeComponent();
  });

  afterEach(() => {
    component = null;
  });


  // Opdracht 0
  it('should render the home component', () => {
    expect(component).toBeTruthy();
  });


  // Opdracht 1 
  it.skip('should have the correct page title', () => {
    expect(component.pageTitle).toBe('snapje');
  });


  // Opdracht 2 
  it.skip('should return the proper string', () => {
    const string = component.testString();
    expect(string).toBe('snapje');
  });


  // Opdracht 3
  it.skip('should add numbers together', () => {
    const result = component.testSum(1, 3);
    expect(result).toBe(0);
  });


  // Opdracht 4
  it.skip.each([
    [1, 2, 0],
    [1, 2, 0],
    [1, 2, 0],
    [1, 2, 0],
  ])('should add %p and %p to be outcome %p', (a, b, outcome) => {
    const result = component.testSum(a, b);
    expect(result).toBe(outcome);
  });


  // Opdracht 5
  it.skip('should return an error string when not a number', () => {

  });


  // Opdracht 6
  it.skip('should change the page title when changeTitle is executed', () => {

  });
});
