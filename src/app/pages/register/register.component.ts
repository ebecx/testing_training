import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router, private userService: UserService) { }

  registerMessage = {message: null, type: null};
  registerDetails = {userName: "", passWord: "", passwordConfirmation: ""};

  ngOnInit(): void {
  }

  register() {
    if(this.validateDetails()) {
      this.userService.registerUser(this.registerDetails).subscribe(
        {
          next: (res) => { this.createUserinDatabase(res) },
          error: (err) => { this.createUserinDatabase(err) }
        });;
    };
  }

  createUserinDatabase(res) {
    console.log(res);

    if(res.status != 200) {
      this.registerMessage = {message: res.statusText, type: "error"};
      return;
    }

    if(res.status === 200) {
      setTimeout(() => { this.backToLoginPage(); }, 1500);
    }
  }

  validateDetails() {

    // Check if fields are not empty
    if(this.registerDetails.userName === "" || this.registerDetails.passWord === "" || this.registerDetails.passwordConfirmation === "") {
      this.registerMessage = {message: "Error registering user. Details are not complete..", type: "error"};
      return false;
    }

    //Check if passwords match
    if(this.registerDetails.passWord != this.registerDetails.passwordConfirmation) {
      this.registerMessage = {message: "Error registering user. Password do not match..", type: "error"};
      return false;
    }

    this.registerMessage = {message: "User created succesfully. Redirecting to loginpage..", type: "success"};
    return true;
  }

  backToLoginPage() {
    this.router.navigate(['/login']);
  }

}
