import { TestBed } from '@angular/core/testing';
import { RegisterComponent } from './register.component';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from 'src/app/app.module';

describe('ModalComponent', () => {
  // Required for functions that have timeouts
  jest.useFakeTimers();
  let fixture;
  let component;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
        RouterTestingModule.withRoutes([]),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture = null;
    router = null;
    component = null;
  });

  it('should render the login component', () => {
    expect(component).toBeTruthy();
  });

  it.skip('should have a default empty register message', () => {

  });

  it.skip('should have a default empty register details object', () => {

  });

  it.skip('should provide an error when details are not complete', () => {

  });

  it.skip('should provide an error when passwords do not match', () => {

  });

  it.skip('should set the registermessage to error when user was not created', () => {

  });

  it.skip('should navigate back to login page when user was created', () => {

  });

});

