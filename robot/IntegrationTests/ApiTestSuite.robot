*** Settings ***
Resource          ApiKeywords.robot
Documentation     Documentation described in the keywords resource file

*** Variables ***
${URL}            https://summerschool-backend.azurewebsites.net/

${NEW_TITLE}      Test titel
${NEW_STATUS}     TODO
${NEW_DUEDATE}    2022-10-20
${NEW_DESCR}      Test beschrijving van todo
${REQUEST}        {"description": "${NEW_DESCR}", "dueDate": "${NEW_DUEDATE}", "status": "${NEW_STATUS}", "title": "${NEW_TITLE}"}

*** Test Case ***
1. Login In The TODO App
    Login The TODO Application      username=VulHierUsername    password=VulHierPassword
    Response Status Should Be       200

2. Get All TODO Items
    GET                             /todo/getAll                headers=${REQUEST_HEADERS}
    Log Response Body To Console

3. Create A New TODO Item With Status In Progress
    &{resp}=                        POST                        /todo/addToDoItem             headers=${REQUEST_HEADERS}    body=${REQUEST}
    Get Latest Created TODO Id      ${resp.body}

4. Get Only The Created TODO Item In Step 3
    &{resp}=                        GET                         /todo/get/${LATEST_ID}        headers=${REQUEST_HEADERS}

# 5. Change Status Of The Latest Created TODO

# 6. Validate The Status Was Correctly Changed In Testcase 5

# 7. Delete The Latest Created TODO

# 8. Logout Of The TODO Application

# 9. Validate The Logout In Step 8 Was Successful
