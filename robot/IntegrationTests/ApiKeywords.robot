*** Settings ***
Library          REST                                                                 ${URL}
Library          String
Documentation    Repo: https://github.com/asyrjasalo/RESTinstance/
...              Keyword documentation: https://asyrjasalo.github.io/RESTinstance/

*** Variables ***


*** Keywords ***
# Logs into the TODO application.
# Sets a variable named ${REQUEST_HEADERS} which should be sent with every subsequent request.
Login The TODO Application
    [Arguments]                  ${username}                  ${password}
    &{resp}=                     POST                         /user/login                        body={"userName": "${USERNAME}", "passWord": "${PASSWORD}"}
    # Response Status Should Be    200
    ${SPLIT}=                    Split String                 ${resp.headers["Set-Cookie"]}      ;
    ${SESSION_COOKIE}=           Set Variable                 ${SPLIT}[0]
    Set Global Variable          ${REQUEST_HEADERS}           {"Cookie": "${SESSION_COOKIE}"}

Response Status Should Be
    [Arguments]                  ${RESP_STATUS}
    Integer                      response status              ${RESP_STATUS}

Log Response Body To Console
    Output                       response body

Verify Response JSON
    [Arguments]                  ${EXPECTED}
    Expect Response              ${EXPECTED}

Verify Response Body
    [Arguments]                  ${EXPECTED}
    String                       response body                ${EXPECTED}

# Sets a global variable named ${LATEST_ID} which can be used in all tests
# Requeres an input of the ${ALL_TODOS} array to determine the latest id
Get Latest Created TODO Id
    [Arguments]                  ${ALL_TODOS}
    ${L}=                        Get Length                   ${ALL_TODOS}
    ${ID}=                       Set Variable                 ${ALL_TODOS}[${L-1}]
    Set Global Variable          ${LATEST_ID}                 ${ID.id}
    Log To Console               Set LATEST_ID to ${ID.id}
