*** Settings ***
Resource                 UIKeywords.robot
Suite Teardown           Close All Browsers
Documentation            Documentation described in the keywords resource file

*** Variable ***
${TEST_URL}              https://summerschool-frontend.azurewebsites.net
# Selenium delay in seconds. Increase to 1 to watch tests run
${SELENIUM_DELAY}        0

${TODO_TITLE}            GUI Test automation
${TODO_DUEDATE}          2021-11-01
${TODO_DESCRIPTION}      GUI test TODO description

${CHANGED_TITLE}         Edited Test automation
${CHANGED_DUEDATAE}      2021-12-01
${CHANGED_DECRIPTION}    Edited test TODO description

*** Test Cases ***
# Validate that tests are running as intended. Remove if RobotFramework is working fine.
# 0. Validate RobotFramework Is Running Correctly
#                        Start Browser Based On Environment                       https://google.com
#                        Wait Until Page Contains Element                         name=q
#                        Log To Console                                           UI tests are working
#                        [Teardown]                                               Close Browser

1. Open The TODO Application And Check Elements
    Start Browser Based On Environment          ${TEST_URL}
    Wait Until Page Contains Element            id=login-button             timeout=5
    Wait Until Page Contains Element            id=username                 timeout=1
    Wait Until Page Contains Element            id=password                 timeout=1

2. Login To The TODO Application
    Wait Until Page Contains Element            id=login-button             timeout=10
    Input Text                                  id=username                 Admin
    Input Text                                  id=password                 Admin
    Click Element                               id=login-button
    Wait Until Page Contains                    Summerschool Todolist

3. Create A New TODO Item
    Wait Until Page Contains Element            id=add-todo                 timeout=2
    Click Element                               id=add-todo
    Wait Until Page Contains Element            id=add-todo-modal           timeout=2
    Input Text                                  id=add-todo-title           ${TODO_TITLE}
    Input Text                                  id=add-todo-duedate         ${TODO_DUEDATE}
    Input Text                                  id=add-todo-description     ${TODO_DESCRIPTION}
    Select From List By Index                   id=add-todo-status          1
    Click Element                               id=add-todo-save
    Wait Until Element Is Not Visible           id=add-todo-modal           timeout=3
    Wait Until Element Is Visible               id=add-todo                 timeout=5

4. Open And Validate The Created TODO Of Step 3
    ${ID}=                                      Get ID For TODO             ${TODO_TITLE}
    Click Element                               id=todo-${ID}
    Wait Until Element Is Visible               id=view-todo-item           timeout=2
    Page Should Contain                         ${TODO_DESCRIPTION}
    Page Should Contain                         ${TODO_DUEDATE}
    Click Element                               id=close-modal
    Wait Until Element Is Not Visible           id=close-modal              timeout=2

5. Edit Created TODO Of Step 3
    Get ID For TODO                             ${TODO_TITLE}
    Wait Until Element Is Visible               id=edit-${ID}               timeout=2
    Click Element                               id=edit-${ID}
    Wait Until Element Is Visible               id=close-modal              timeout=2
    Input Text                                  id=edit-todo-title          ${CHANGED_TITLE}
    Input Text                                  id=edit-todo-duedate        ${CHANGED_DUEDATAE}
    Input Text                                  id=edit-todo-description    ${CHANGED_DECRIPTION}
    Select From List By Index                   id=edit-todo-status         2
    Wait Until Element Is Visible               id=edit-todo-save           timeout=3
    Click Element                               id=edit-todo-save
    Wait Until Element Is Visible               id=todo-${ID}               timeout=2

6. Validate Edited TODO Was Successfully Changed In Step 5
    TODO Should Be Visible On Page              ${CHANGED_TITLE}
    ${ID}=                                      Get ID For TODO             ${CHANGED_TITLE}
    Click Element                               id=todo-${ID}
    Wait Until Element Is Visible               id=view-todo-item           timeout=2
    Page Should Contain                         ${CHANGED_DECRIPTION}
    Page Should Contain                         ${CHANGED_DECRIPTION}
    Click Element                               id=close-modal
    Wait Until Element Is Not Visible           id=close-modal              timeout=2

7. Remove Created And Edited Todo
    ${ID}=                                      Get ID For TODO             ${CHANGED_TITLE}
    Wait Until Element Is Visible               id=delete-${ID}             timeout=3
    Click Element                               id=delete-${ID}
    Wait Until Page Does Not Contain Element    id=todo-${ID}               timeout=2
