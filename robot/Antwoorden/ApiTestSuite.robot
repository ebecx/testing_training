*** Settings ***
Resource         ApiKeywords.robot
Documentation    Documentation described in the keywords resource file

*** Variables ***
${URL}           https://summerschool-backend.azurewebsites.net/

*** Test Case ***
1. Login In The TODO App
    Login The TODO Application            username=Admin                               password=Admin
    Response Status Should Be             200

2. Get All TODO Items
    GET                                   endpoint=todo/getAll                         headers=${REQUEST_HEADERS}
    Response Status Should Be             200

3. Create A New TODO Item With Status In Progress
    &{resp}=                              POST                                         endpoint=todo/addToDoItem                    body={"description": "Nieuwste TODO", "dueDate": "2021-11-14", "status": "INPROGRESS", "title": "Nieuwste"}    headers=${REQUEST_HEADERS}
    Response Status Should Be             200
    Get Latest Created Training Number    ${resp.body}

4. Get Only The Created TODO Item In Step 3
    &{resp}=                              GET                                          endpoint=todo/get/${LATEST_ID}               headers=${REQUEST_HEADERS}
    Should Be Equal As Strings            ${resp.body.title}                           Nieuwste
    Should Be Equal As Strings            ${resp.body.description}                     Nieuwste TODO
    Should Be Equal As Strings            ${resp.body.status}                          INPROGRESS
    Should Be Equal As Strings            ${resp.body.dueDate}                         2021-11-14

5. Change Status Of The Latest Created TODO
    &{resp}=                              PUT                                          endpoint=todo/updateToDoItem/${LATEST_ID}    body={"description": "Updated TODO", "dueDate": "2022-11-14", "status": "DONE", "title": "Updated"}            headers=${REQUEST_HEADERS}

6. Validate The Status Was Correctly Changed In Testcase 5
    &{resp}=                              GET                                          endpoint=todo/get/${LATEST_ID}               headers=${REQUEST_HEADERS}
    Should Be Equal As Strings            ${resp.body.title}                           Updated
    Should Be Equal As Strings            ${resp.body.description}                     Updated TODO
    Should Be Equal As Strings            ${resp.body.status}                          DONE
    Should Be Equal As Strings            ${resp.body.dueDate}                         2022-11-14
    Log Response Body To Console

7. Delete The Latest Created TODO
    DELETE                                endpoint=todo/deleteToDoitem/${LATEST_ID}    headers=${REQUEST_HEADERS}
    Response Status Should Be             200
    Log To Console                        Deleted TODO ${LATEST_ID}
    GET                                   todo/get/${LATEST_ID}
    Response Status Should Be             401

8. Logout Of The TODO Application
    POST                                  endpoint=user/logout                         headers=${REQUEST_HEADERS}
    Response Status Should Be             200

9. Validate The Logout In Step 8 Was Successful
    GET                                   endpoint=todo/getAll                         headers=${REQUEST_HEADERS}
    Response Status Should Be             401