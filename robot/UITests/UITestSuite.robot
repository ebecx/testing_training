*** Settings ***
Resource                 UIKeywords.robot
Suite Teardown           Close All Browsers
Documentation            Documentation described in the keywords resource file

*** Variable ***
${TEST_URL}              https://summerschool-frontend.azurewebsites.net
# Selenium delay in seconds. Increase to 1 to watch tests run
${SELENIUM_DELAY}        0

${TODO_TITLE}            GUI Test automation
${TODO_DUEDATE}          2021-11-01
${TODO_DESCRIPTION}      GUI test TODO description

${CHANGED_TITLE}         Edited Test automation
${CHANGED_DUEDATAE}      2021-12-01
${CHANGED_DECRIPTION}    Edited test TODO description

*** Test Cases ***
# Validate that tests are running as intended. Remove if RobotFramework is working fine.
0. Validate RobotFramework Is Running Correctly
    Start Browser Based On Environment          https://google.com
    Wait Until Page Contains Element            name=q
    Log To Console                              UI tests are working
    [Teardown]                                  Close Browser

# 1. Open The TODO Application And Check Elements
    # Start Browser Based On Environment          ${TEST_URL}

# 2. Login To The TODO Application

# 3. Create A New TODO Item

# 4. Open And Validate The Created TODO Of Step 3

# 5. Edit Created TODO Of Step 3

# 6. Validate Edited TODO Was Successfully Changed In Step 5

# 7. Remove Created And Edited Todo