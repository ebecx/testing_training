*** Settings ***
Library          SeleniumLibrary
Library          String
Documentation    Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html

*** Variable ***
${BROWSER}       chrome

*** Keywords ***
# Starts browser defined in variables. This allows Docker to set browser when running in container
Start Browser Based On Environment
    [Arguments]                      ${URL}
    Set Selenium Speed               ${SELENIUM_DELAY}
    Open Browser                     url=${URL}                                      browser=${BROWSER}

# Validates TODO is visible on page based on ${TODO_TITLE} input argument
TODO Should Be Visible On Page
    [Arguments]                      ${TODO_TITLE}
    Wait Until Element Is Visible    xpath=//*[contains(text(), "${TODO_TITLE}")]    timeout=5

# Sets a global variable ${LATEST_ID} based on the ${TODO_TITLE} input argument
Get ID For TODO
    [Arguments]                      ${TODO_TITLE}
    ${FULL_ID}=                      Get Element Attribute                           xpath=//*[contains(text(), "${TODO_TITLE}")]    id
    ${SPLIT}=                        Split String                                    ${FULL_ID}                                      -
    Set Global Variable              ${LATEST_ID}                                    ${SPLIT}[1]
    Log To Console                   Set LATEST_ID to ${LATEST_ID}
