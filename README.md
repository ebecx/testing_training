# NS training
Deze repository bevat de Angular frontend met Jest unit tests, en RobotFramework GUI & Integratie tests

### Live varianten:
- frontend link: https://summerschool-frontend.azurewebsites.net/
- backend link (Swagger): https://summerschool-backend.azurewebsites.net/swagger-ui/index.html#/
___

# Frontend

## Installatie
1. Download de Node.js LTS versie via https://nodejs.org/en/
2. Installeer Node.js
3. Download een IDE als Visual Studio Code
4. Open dit project in Visual Studio Code
5. Voer `npm install` uit in de terminal

## Development server
_Deze stap heb je niet nodig om alleen unit tests te draaien_\
Voer `npm run start` uit voor een dev server. Navigeer naar `http://localhost:4200/`

## Unit testing opdracht
De unit tests zitten in `.spec.ts` files. Op dit moment zitten er 4 unit test files in deze repository. \
Begin bij nr 1 met de opdrachten. De moeilijkheidsgraad zal per test verhoogd worden. \
Voorbeeld:  `src/app/login.component.spec.ts`
1.  `src/app/app.component.spec.ts`
2.  `src/app/pages/home/home.component.spec.ts`
3.  `src/app/pages/register/register/component.spec.ts`

### Alle tests uitvoeren
Draai `npm run test` om alle tests uit te voeren

### Een testfile draaien
Voer `npx jest FILENAME --coverage` uit

### Een test in een testfile draaien
1. Zet `it.only` in je unit test file
2. `npx jest FILENAME --coverage`

## Tips

- Na het draaien van de unit tests zal er een `coverage` folder worden gegenereerd. Open daar de `index.html` en je krijgt een coverage overzicht



# Integratie en UI tests
RobotFramework tests

## Installatie Windows

1.	Download en installeer de laatste versie van Python 3.X. Verderop in de handleiding heb je de locatie van de installatie nog nodig, vergeet deze dus niet. __Vink ook aan dat Python moet worden toegevoegd aan PATH!__
2.	Nu voor de installatie van RobotFramework, voer de volgende commands uit in je command prompt:
    1.	`pip install robotframework`
    2.	`pip install RESTinstance`
    3.	`pip install robotframework-seleniumlibrary`


3.	Als laatste de installatie voor de automatisering van de browser. Voor elke browser heb je een andere “Driver” nodig, bijvoorbeeld Chrome gebruikt Chromedriver, Firefox gebruikt Geckodriver enz. 

    1.	Voor deze installatie gebruiken we Chromedriver. Ga naar https://chromedriver.chromium.org/ en download de laatste versie van Chromedriver voor Windows. Kies de “stable release” optie zoals hieronder aangegeven:  
    2.	Na het downloaden pak je dit .zip bestand uit in je installatie map van Python. Als je niets hebt aangepast is dat `%USERPROFILE%\AppData\Local\Programs\Python\Python*`

4.	Nu kan je jouw favoriete IDE support geven voor .robot files. 
Voor Visual Studio Code is het relatief simpel:
    1.	Dowload de volgende extensies: “RobotF extension” en “Robot Framework Intellisense”.
    2.	Ga naar File > Preferences > Settings > Robot Framework Intellisense
    3.	Open de volgende instelling: 
    4.	Hier voeg je het volgende toe aan je settings.json:\
        `"rfLanguageServer.libraries": [
            "BuiltIn-3.0.4",
            "SeleniumLibrary-3.3.1"
            ]`

5.	In je .robot files heb je nu autocomplete voor SeleniumLibrary en met de toetscombinatie __“ALT + SHIFT + F”__ kan je de robot bestanden formatten.

## Tests lokaal draaien

Als je de installatie handleiding goed hebt gevolgd, kan je tests draaien met het volgende command:
`robot testSuite.robot`

Het kan zijn dat de installatie niet goed is gegaan. Je kan ook proberen de tests te draaien met command: 
`python3 -m robot testSuite.robot`

Je kan er ook voor kiezen hele folders te draaien. Dan gaan alle testsuites in die folder draaien:
`robot testFolder`

## Reports

RobotFramework output standaard een aantal report files. Als je RobotFramework tests lokaal draait, zullen de reports gegenereerd worden in de folder vanuit waar je ze uitvoert. Open de report.html voor een mooi overzicht. 
