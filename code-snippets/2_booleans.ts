/*
  Pas de booleans zo aan zodat de console.assert functie geen error meer geeft
*/

// Niet aanpassen
let waar = true;
let onwaar = false;

let welwaar = !false;
let nietwaar = !true;


// Pas parameters in de console.assert functie aan
console.assert(waar === nietwaar);
