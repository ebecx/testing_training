/*
  Pas de if-else statement op de juiste manier aan zodat de assertion slaagt
*/

// Aanpassen
let leeftijd = 12;

// Niet aanpassen
let toegangVerleend;
let identificatieVerpicht;

if(leeftijd < 18) {
  toegangVerleend = false;
  identificatieVerpicht = false;
}
else if(leeftijd >= 18 && leeftijd <= 25) {
  toegangVerleend = true;
  identificatieVerpicht = true;
}
else if(leeftijd > 25) {
  toegangVerleend = true;
  identificatieVerpicht = false;
}

// Geef de juiste leeftijd op zodat de assertion slaagt
console.assert(toegangVerleend === !identificatieVerpicht);
