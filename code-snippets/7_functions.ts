/*
  Functions
*/

// Niet aanpassen
function sayHi() {
  return 'Hello World!';
}

// Aanpassen
console.assert(sayHi() === 'Hallo world!');


// Niet aanpassen
function addAndSubstract(number1, number2, number3) {
  return number1 + number2 - number3;
}

// Aanpassen
console.assert(addAndSubstract(0, 0, 0) === 28)
console.assert(addAndSubstract(0, 0, 0) === 35)
console.assert(addAndSubstract(0, 0, 0) === -20)
console.assert(addAndSubstract(0, 0, 0) === 99.99)


// Niet aanpassen
function addTaxes(price, tax) {
  let taxAmount = price * tax;
  return price + taxAmount;
}

// Aanpassen
console.assert(addTaxes(1.99, 0.21) === ?)
console.assert(addTaxes(300, 0.21) === ?)
console.assert(addTaxes(197.50, 0.21) === ?)
console.assert(addTaxes(24.75, 0.21) === ?)
