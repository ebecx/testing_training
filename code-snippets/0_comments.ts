// Dit is een voorbeeld van code commentaar

// Door 2 slashes toe te voegen aan het begin van een regel, kan je ervoor zorgen dat je commentaar kant typen bij code

// Alles wat als commentaar staat aangemerkt wordt volledig genegeerd door de compiler / transpiler etc.

/* Dit is nog een voorbeeld van commentaar, maar net iets anders. Dit commentaar block werkt met een openingstag en een sluitingstag */
