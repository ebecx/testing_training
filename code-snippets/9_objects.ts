/*
  Werken met objects
 */

// Niet aanpassen
const person = {
  firstName: 'John',
  lastName : 'Doe',
  id       : 5566,
  fullName : function() {
    return this.firstName + ' ' + this.lastName;
  }
};

// Los de volgende vergelijkingen op
console.assert(changeMe === 'John');
console.assert(changeMe === 'Doe');
console.assert(changeMe === 5566);
console.assert(changeMe === 'John Doe');



