/*
  Werken met arrays.
 */

// Niet aanpassen
let fruits = ['Apple', 'Banana', 'Cantaloupe'];

// Voeg 'Durian' toe aan bovenstaande array


// Niet aanpassen
console.log(fruits);
console.assert(fruits.includes('Durian'));

// Haal 'Apple' uit de array


// Niet aanpassen
console.log(fruits);
console.assert(!fruits.includes('Apple'));

// Controleer de 'lengte' van de array en stop deze in de variabele 'size'
let size = 0;

// Niet aanpassen
console.assert(size === 3);

