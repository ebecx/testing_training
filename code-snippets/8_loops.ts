/*
  Loop, Loops, Looping
  Pas de for-loop aan zodat je dit figuur in je terminal te zien krijgt:

#
##
###
####
#####
######
#######

*/

// Niet aanpassen
let hekje = '#';

// Aanpassen
for(let x = 0; x < 2; x++) {
  console.log(hekje);
}
