/*
  Geef het getal de juiste waarde om de assertion te laten slagen
*/

// Aanpassen
let number = 1;

// Niet aanpassen
let array = [];

switch(number) {
  case 1: array.push('Hoe');
    break;
  case 2: array.push('kan');
    break;
  case 3: array.push('het');
    break;
  case 4: array.push('zo');
    break;
  case 5: array.push('zijn');
    break;
  default: array.push('Niet!');
}

// Zorg dat de assertion slaagt
console.assert(array[0] === 'Niet!');
