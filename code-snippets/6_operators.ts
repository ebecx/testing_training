/*
  Werken met operators
 */

// Niet aanpassen
let a = 10;
let b = 15;

// * - / + %
// Gebruik 1 operator (op de plek van het vraagteken) om tot het gewenste antwoord te komen.
console.assert(a + b === 25);
console.assert(a - b === -5);
console.assert(a * b === 150);
console.assert(b / a === 1.5);
console.assert(b / a === 5); // Maar gebruik geen -

// == != > < >= <= ===
// Gebruik 1 operator (op de plek van het vraagteken) om tot het gewenste antwoord te komen.
console.assert(10 ? 10);
console.assert(10 ? 37);
console.assert(10 ? 11);
console.assert(10 ? "10");
console.assert(10 ? 9);

// && || !
// Gebruik 1 operator (op de plek van het vraagteken) om tot het gewenste antwoord te komen.
console.assert(true ? true);
console.assert(true ? false);
console.assert(false ? true);
console.assert(?false);

